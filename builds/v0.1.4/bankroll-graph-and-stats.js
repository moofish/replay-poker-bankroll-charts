const recordsUrl = 'https://www.replaypoker.com/api/bank_records?page={page}'

window.addEventListener('load', () => {
	fetchRecords().then(records => {
		injectStats(records)
		injectCSVDownloadLink(records)
		injectChart(records)
	})
})

/*
 * Fetch implementation
 */

function fetchRecords(page=1, fetched=[]) {
	console.log('Fetch bankroll records', '|', 'Page', page, '|', 'Total', fetched.length)

	return fetch(recordsUrl.replace('{page}', page)).then(resp => {
		return resp.json()
	}).then(newRecords => {
		if(newRecords.length == 0) {
			return storeRecords(fetched)
		} else {
			const keyOrder = Object.keys(newRecords[0] || {})
			const newRecordsJson = newRecords.map(r => JSON.stringify(r, keyOrder))

			return getLastSeenRecord().then(lastSeen => {
				const lastSeenJson = JSON.stringify(lastSeen, keyOrder)       // passing keyOrder makes json output comparable
				const lastSeenPosition = newRecordsJson.indexOf(lastSeenJson) // e.g. with indexOf()!

				if(lastSeenPosition === -1) { // haven't seen the last checkpoint yet, keep going
					return fetchRecords(page + 1, fetched.concat(newRecords))
				} else { // we've seen one of these records before, stop fetching and load the rest from the cache
					return joinRecordsFromCache([...fetched, ...newRecords.slice(0, lastSeenPosition)]).then(all => {
						return storeRecords(all)
					})
				}
			})
		}
	})
}

/*
 * Record caching
 */

function getRecordsFromCache() {
	return storageGet('records')
}

function getLastSeenRecord() {
	return storageGet('lastSeenRecord')
}

function storeRecords(records) {
	return storageSet('lastSeenRecord', records[0]).then(() => {
		return storageSet('records', records)
	})
}

function joinRecordsFromCache(records) {
	return getRecordsFromCache().then(storedRecords => {
		return records.concat(storedRecords)
	})
}

/*
 * Extension storage helpers
 */

function storageGet(key) {
	return new Promise((resolve, reject) => {
		chrome.storage.local.get([key], result => {
			if(chrome.runtime.lastError) {
				const errorMsg = chrome.runtime.lastError.message
				console.error('Bankroll Extension', '|', 'Storage error', errorMsg)
				reject(errorMsg)
			} else {
				resolve(result[key])
			}
		})
	})
}

function storageSet(key, value) {
	return new Promise((resolve, reject) => {
		const update = {}
		update[key] = value

		chrome.storage.local.set(update, () => {
			if(chrome.runtime.lastError) {
				const errorMsg = chrome.runtime.lastError.message
				console.error('Bankroll Extension', '|', 'Storage error', errorMsg)
				reject(errorMsg)
			} else {
				resolve(value)
			}
		})
	})
}

/*
 * CSV download
 */

function injectCSVDownloadLink(records) {
	const link = document.createElement('a')
	link.onclick = () => downloadBankHistoryCSV(records)

	link.setAttribute('style', 'display:block; padding-bottom:1em; text-align:right')
	link.innerText = 'Download CSV'

	document.querySelector('h1').insertAdjacentElement('afterend', link)
}

function downloadBankHistoryCSV(records) {
	const data = buildBankHistoryCSV(records)
	const filename = `replay-bankroll-history-${new Date().toISOString()}.csv`
	downloadFile(data, 'text/csv', filename)
}

function downloadFile(data, mimeType, filename) {
	const blobUrl = window.URL.createObjectURL(
		new Blob([data], { type: mimeType })
	)

	const a = document.createElement('a')
	a.setAttribute('download', filename)
	a.setAttribute('href', blobUrl)
	a.setAttribute('style', 'display:none')

	document.body.appendChild(a)
	a.click()
	document.body.removeChild(a)

	window.URL.revokeObjectURL(blobUrl)
}

function buildBankHistoryCSV(records) {
	const headings = ["Date", "Total", "Change +/-", "Reason", "Description"]
	const rows = [headings]

	for(const record of records) {
		const row = [
			new Date(record.date).toLocaleString(),
			record.total,
			record.operation == 'withdraw' ? -record.chips : record.chips,
			record.reason,
			record.description
		]

		rows.push(row)
	}

	return rows.map(encodeCSVRow).join("\r\n")
}

function encodeCSVRow(columns=[]) {
	return columns.map(encodeCSVComponent).join(',')
}

function encodeCSVComponent(val) {
	if(val == null) {
		return ''
	} else {
		const str = `${val}`
		if(str.includes(',') || str.includes('"') || str.includes("\r") || str.includes("\n")) {
			return '"' + str.replace(/"/g, '""') + '"'
		} else {
			return str
		}
	}
}

/*
 * Charting
 */

function injectChart(records) {
	const dataPoints = records.map(record => {
		return [record.date, record.total]
	})

	const script = document.createElement('script')
	script.innerHTML = `
		// Render functions are copied onto the page, where they can load and use Google Charts
		${_renderBankrollChart.toString()};
		${_insertChartElement.toString()};
		${_populateChart.toString()};

		// Call _renderBankrollChart on the page, passing a copy of dataPoints
		_renderBankrollChart(${JSON.stringify(dataPoints)})
	`

	document.body.appendChild(script)
}

/*
 * These functions are injected as source code onto the Replay website in order to load and use the Google Charts API
 *
 * This is the simplest way I know to work around extension sandbox limitations, without bundling a large chart library
 * which would make the extension harder to verify and trust
 */

function _renderBankrollChart(points /* [[date, balance], [date2, balance2]] */) {
	const script = document.createElement('script')
	script.setAttribute('src', 'https://www.gstatic.com/charts/loader.js')

	script.onload = () => {
		google.charts.load('current', {'packages': ['corechart']})

		google.charts.setOnLoadCallback(() => {
			const element = _insertChartElement()
			const render = () => _populateChart(element, points)

			render()

			window.addEventListener('resize', debounce(render))

			function debounce(fn, ms=250) {
				let timeout
				return () => {
					if(timeout) clearTimeout(timeout)
					timeout = setTimeout(fn, ms)
				}
			}
		})
	}

	document.body.appendChild(script)
}

function _insertChartElement() {
	const el = document.createElement('div')
	el.setAttribute('style', 'width:100%; height:400px; padding:1em 0em')
	document.querySelector('h1').insertAdjacentElement('afterend', el)
	return el
}

function _populateChart(element, points) {
	console.log('Populate chart', points.length)

	const dataTable = new google.visualization.DataTable()
	dataTable.addColumn('datetime', 'Date')
	dataTable.addColumn('number', 'Chips')

	dataTable.addRows(points.map(point => [
		new Date(point[0]), point[1]
	]))

	const opts = {
		backgroundColor: '#172633',
		colors: ['#fe9000'],
		theme: 'maximized',
		hAxis: {title: '', textStyle: {color: 'white'}, format: 'MMM d y', gridlines: {count: 12, color: '#555'}, minorGridlines: {count: 0}, showTextEvery: 1},
		vAxis: {title: '', textStyle: {color: 'white'}, format: 'short', gridlines: {count: 12, color: '#555'}, minorGridlines: {count: 0}, showTextEvery: 1},
		legend: {position: 'none'},
		trendlines: {0: { color: '#ffc040'}}
	}

	const chart = new google.visualization.LineChart(element)
	chart.draw(dataTable, opts)
}

/*
 * Statistics
 */

function injectStats(records) {
	const mttAnalysis = analyzeMTTs(records)
	const mttInTheMoneyPercent = mttAnalysis.numInTheMoney / mttAnalysis.numPlayed * 100

	const stats = {
		'MTTs + SnGs Played': mttAnalysis.numPlayed,
		'MTTs + SnGs In The Money': `${mttAnalysis.numInTheMoney} (${mttInTheMoneyPercent.toFixed(2)}%)`,
		'MTTs + SnGs Net Profit':   `${mttAnalysis.netProfit .toLocaleString()} chips`,
		'MTTs + SnGs Total Buy In': `${mttAnalysis.buyInTotal.toLocaleString()} chips`
	}

	const outputElement = document.createElement('table')
	outputElement.className = 'table'
	outputElement.style = 'table-layout:fixed'
	outputElement.innerHTML = Object.entries(stats).map(([ key, value ]) => {
		return `
			<tr>
				<th>${key}</th>
				<td>${value}</td>
			</tr>`
	}).join('')

	document.querySelector('h1').insertAdjacentElement('afterend', outputElement)
}

function analyzeMTTs(records) {
	return records.reduce((analysis, { description='', chips=0 }) => {
		if(description.startsWith('Register')) {
			analysis.numPlayed++;
			analysis.netProfit -= chips
			analysis.buyInTotal += chips
		} else if(description.startsWith('Unregister')) {
			analysis.numPlayed--;
			analysis.netProfit += chips
		} else if(description.startsWith('Prize')) {
			analysis.numInTheMoney++;
			analysis.netProfit += chips
		}
		return analysis
	}, { numPlayed: 0, numInTheMoney: 0, netProfit: 0, buyInTotal: 0 })
}
